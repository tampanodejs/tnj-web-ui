/**
 * @module environments.conf.js
 * @description constants for different build types
 */
var pkg = require('../package.json');

module.exports = {
  environments: {
    local: {
      'environment': {
        'ENV_TYPE': 'working',
        'API_PATH': 'http://localhost:1337/',
        'VERSION': 'v ' + pkg.version
      }
    },
    dev: {
      'environment': {
        'ENV_TYPE': 'dev',
        'API_PATH': '',
        'VERSION': 'v ' + pkg.version
      }
    }
  }
};
