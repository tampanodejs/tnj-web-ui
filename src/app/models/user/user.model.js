/**
 * @file User model
 */
angular.module('tnjRunners.models.user', [])
  .factory('User', userImpl);

/**
 * @method userImpl
 * @description model implementation
 * @returns {Function}
 */
function userImpl() {

  /**
   * @class User
   * @constructor
   * @param {object} dto
   */
  var User = function(dto) {
    this.email = '';
    this.password = '';
    this.username = '';

    if (dto) {
      this.fromDto(dto);
    }
  };

  /**
   * @method fromDto
   * @description converts object to User
   * @param {object} dto
   */
  User.prototype.fromDto = function(dto) {
    var user = this;
    user.email = dto.email;
    user.password = dto.password;
    user.username = dto.username;
  };

  return User;
}
