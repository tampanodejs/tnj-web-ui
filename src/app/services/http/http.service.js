﻿/**
 * @file wrapper for the $http service
 */
angular.module('tnjRunners.services.http', [])
  .service('httpService', httpServiceImpl);

/**
 * @method httpServiceImpl
 * @description service implementation
 * @param {object} $http
 * @param {object} $q
 */
function httpServiceImpl($http, $q) {
  'use strict';

  /**
   * @description service implementation
   */
  return {
    all: all,
    get: get,
    post: post
  };

  /**
   * @method all
   * @description resolve all requests
   * @param {Array} requests
   * @returns {*}
   */
  function all(requests) {
    var defer = $q.defer();

    $q.all(requests)
      .then(
      function(response) {
        // return collection of data
        return defer.resolve(response);
      },
      function(response) {
        return defer.reject(null);
      });

    return defer.promise;
  }

  /**
   * @method get
   * @description handles GET operations
   * @param {string} endpoint - request endpoint
   * @param {object} params - request params
   * @param {function} transformCallback - callback function to transform response
   * @returns {promise}
   */
  function get(endpoint, params, transformCallback) {
    return http('GET', endpoint, null, params, transformCallback);
  }

  /**
   * @method post
   * @description handles POST operations
   * @param {string} endpoint - request endpoint
   * @param {object} request - body request object
   * @param {function} transformCallback - callback function to transform response
   * @returns {promise}
   */
  function post(endpoint, request, transformCallback) {
    return http('POST', endpoint, request, null, transformCallback);
  }

  /**
   * @private
   * @method http
   * @description submits request via $http
   * @param {string} method - HTTP method
   * @param {string} endpoint - request endpoint
   * @param {object} request - body request object (POST, etc..)
   * @param {object} params - request params (GET)
   * @param {function} transformCallback - callback function to transform response
   * @returns {promise}
   */
  function http(method, endpoint, request, params, transformCallback) {
    var defer = $q.defer();

    $http({
      method: method,
      params: params,
      data: request,
      url: endpoint,
      transformResponse: transformCallback
    })
      .success(function(data, status, headers, config) {
        defer.resolve(data);
      })
      .error(function(data, status, headers, config) {
        defer.reject(null);
      });

    return defer.promise;
  }
}
