﻿/**
 * @file get metadata required on app boot
 */
angular.module('tnjRunners.services.metadata', [])
    .service('metadataService', metadataServiceImpl);

/**
 * @method metadataServiceImpl
 * @description service implementation
 * @param {object} httpService
 */
function metadataServiceImpl(httpService) {
  'use strict';

  /**
   * @description service contract
   */
  return {
    primeApp: primeApp
  };

  /**
   * @method primeApp
   * @description fetch required data for the app
   * @returns {promise}
   */
  function primeApp() {
    var requests = [
      // one
      //httpService.get(ENDPOINTS.someFn, { }),
      // two
      //httpService.get(ENDPOINTS.someFn, { })
    ];

    return httpService.all(requests);
  }
}
