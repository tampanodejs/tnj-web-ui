/**
 * @module signInService
 * @file service to sign in / sign up
 */
angular.module('tnjRunners.services.signIn', [
  'tnjRunners.services.http',
  'tnjRunners.constant',
  'tnjRunners.config.path'
])
  .service('signInService', signInServiceImpl);

/**
 * @method signInServiceImpl
 * @description service implementation
 * @param {object} httpService
 * @param {object} endpoints
 * @param {object} environment
 */
function signInServiceImpl(httpService, endpoints, environment) {
  'use strict';

  /**
   * @description service contract
   */
  return {
    signIn: signIn,
    signUp: signUp
  };

  /**
   * @method signIn
   * @description signs in a user
   * @param {User} user
   * @returns {promise}
   */
  function signIn(user) {
    var url = environment.API_PATH + endpoints.USER_SIGN_IN;
    return httpService.post(url, user, null);
  }

  /**
   * @method signUp
   * @description creates user account / signs in
   * @param {User} user
   * @returns {promise}
   */
  function signUp(user) {
    var url = environment.API_PATH + endpoints.USER_SIGN_UP;
    return httpService.post(url, user, null);
  }
}
