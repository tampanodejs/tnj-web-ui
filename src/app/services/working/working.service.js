/**
 * @file service to demo working behavior
 */
angular.module('tnjRunners.services.working', [])
  .service('workingService', workingServiceImpl);

/**
 * @method workingServiceImpl
 * @description service implementation
 * @param {object} $q
 * @param {function} $timeout
 */
function workingServiceImpl($q, $timeout) {
  'use strict';

  /**
   * @description service contract
   */
  return {
    mockAsync: mockAsync
  };

  /**
   * @method doSomething
   * @description mock async operation
   * @param {number} delay
   *  milliseconds to delay
   */
  function mockAsync(delay) {
    var defer = $q.defer();

    $timeout(function() {
      defer.resolve(true);
    }, delay || 2000);

    return defer.promise;
  }
}
