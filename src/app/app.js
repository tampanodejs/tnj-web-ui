/**
 * @module app.js
 * @file entry point for the application
 */
angular.module('tnjRunners', [
  /* angular */
  'ngAnimate',
  'ngSanitize',
  /* vendor */
  'ui.router',
  'angular-loading-bar',
  /* app */
  'tnjRunners.templates',
  'tnjRunners.constant',
  'tnjRunners.config.path',
  'tnjRunners.directive.header',
  'tnjRunners.directive.nav',
  'tnjRunners.directive.footer',
  'tnjRunners.controller.landing',
  'tnjRunners.controller.gettingStarted',
  'tnjRunners.services.loader'
])

/**
 * run block
 */
  .run(runImpl)

/**
 * config block
 */
  .config(configImpl);

/**
 * @method runImpl
 * @description run implementation details
 * @param {object} $rootScope
 * @param {object} $state
 * @param {object} $stateParams
 */
function runImpl($rootScope, $state, $stateParams) {
  /**
   * @see ui-router - https://github.com/angular-ui/ui-router/blob/master/sample/app/app.js
   *
   * It's very handy to add references to $state and $stateParams to the $rootScope
   * so that you can access them from any scope within your applications.For example,
   * <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
   * to active whenever 'contacts.list' or one of its descendants is active.
   */
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
}

/**
 * @method configImpl
 * @description config implementation details
 * @param {object} environment
 * @param {object} states
 * @param {object} $stateProvider
 * @param {object} $urlRouterProvider
 * @param {object} $compileProvider
 */
function configImpl(environment, states, $stateProvider, $urlRouterProvider,
                    $compileProvider) {
  /**
   * @see https://docs.angularjs.org/api/ng/provider/$compileProvider
   * Call this method to enable/disable various debug runtime information in the compiler
   * If you wish to debug an application with this information then you should open up a
   * debug console in the browser then call this method directly in this console:
   * angular.reloadWithDebugInfo();
   */
  if (environment.ENV_TYPE === 'working') {
    $compileProvider.debugInfoEnabled(false);
  }

  /**
   * define abstract state
   */
  $stateProvider
    .state({
      name: states.ABSTRACT,
      abstract: true,
      views: {
        'current': {
          // loaded from current state
        }
      },
      resolve: {
        appPrime: appPrimeImpl
      }
    });

  /**
   * redirect to landing page
   */
  $urlRouterProvider.otherwise('/landing');
}

/**
 * @method appPrimeImpl
 * @description implementation details
 *  primes the application and turns off splash screen
 * @param {object} loaderService
 * @param {function} $timeout
 * @param {object} $log
 */
function appPrimeImpl(loaderService, $timeout, $log) {
  // todo: replace with actual application prime
  return $timeout(function() {
    loaderService.setPriming(false);
    $log.debug('app primed');
  }, 1000);
}
