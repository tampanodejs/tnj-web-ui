﻿/**
 * @file stores constant values for the application
 * @see https://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
 *  Use NAMES_LIKE_THIS for constant values
 */

angular.module('tnjRunners.constant', [])

/**
 * @constant app
 */
  .constant('app', {
    NAME: 'Tampa Node.js Runners'
  })

/**
 * @constant endpoints
 */
  .constant('endpoints', {
    USER_SIGN_IN: 'user/signIn',
    USER_SIGN_UP: 'user/signUp'
  })

/**
 * @constant enums
 */
  .constant('enums', {})

/**
 * @constant settings
 */
  .constant('settings', {})

/**
 * @constant states
 */
  .constant('states', {
    ABSTRACT: 'tnjRunners',
    LANDING: 'tnjRunners.landing',
    GETTING_STARTED: 'tnjRunners.gettingStarted'
  })

/**
 * @constant strings
 */
  .constant('strings', {})

/**
 * @constant templates
 */
  .constant('templates', {});
