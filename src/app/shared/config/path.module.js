/* auto generated via gulp ngConstant task */
// jscs:disable
/* jshint ignore:start */

angular.module("tnjRunners.config.path", [])

.constant("environment", {
 "ENV_TYPE": "working",
 "API_PATH": "http://localhost:1337/",
 "VERSION": "v 1.0.0"
})

;
// jscs:enable
/* jshint ignore:end */
